*USO DE PLANTILLA PREEXISTENTE*

La plantilla original de HTLM, tiene como nombre Perfect-learn, su modelo y estructura está dentro de la categoría de Educación, su idioma programa está en inglés y contiene tres páginas en HTLM.

*CAMBIOS REALIZADOS A PARTIR DE LA PLANTILLA BASE*

Las modificaciones que se realizaron dentro de la plantilla fueron, primeramente, adecuarla para una página de formación educativa, cambiamos todos los apartados con el idioma español, seleccionamos botones y los personalizamos para que se acoplen al nombre que le asignamos.

Dentro de la interfaz principal le asignamos un nombre de “Aula Tecnológica” el cual llega a ser como nombre oficial de la página.

Personalizamos la plantilla con temas de: cursos, capacitaciones y recursos tecnológicos educativos.

Introducimos textos e imágenes educativos para que ayuden al futuro usuario a familiarizarse con toda la interfaz de navegación.

Agregamos Iconos seleccionables para que el usuario sepa distinguir y seleccionar contenidos educativos de acuerdo con el nivel educativo que se encuentre.
